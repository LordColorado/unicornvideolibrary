var express = require('express');
var app = express();

var dataContext = require('./dataContext');
var fs = require('fs');

// Default welcome
app.get('/', function (req, res) {
   fs.readFile('App_Data/unicorn.txt', 'utf8', function (err, data) {
      res.end(data);
   });
})

// Get list of videos
app.get('/videos', function (req, res) {
   dataContext.readFromStorage(function (videos) {
      res.end(JSON.stringify(videos));
   });
})

// Get video detail
app.get('/videos/:id', function (req, res) {
   dataContext.readFromStorage(function (videos) {
      if (req.params.id in videos) {
         res.end(JSON.stringify(videos[req.params.id]));
      } else {
         res.sendStatus(404);
      }
   });
})

// Create video
app.post('/videos', function (req, res) {

})

// Update video
app.patch('/videos/:id', function (req, res) {

})

// Delete video
app.delete('/videos/:id', function (req, res) {

})

var server = app.listen(8081, function () {
   var host = server.address().address
   var port = server.address().port

   console.log("UnicornVideoLibrary running on http://%s:%s", host, port)
})