var fs = require("fs");

//var DataContext = DataContext || {
module.exports = {
    videos: {},

    getAllVideos(callback) {
        if (this.videos == null || Object.keys(this.videos).length == 0)
        {
            this.readFromStorage(callback);
        }

        callback(videos);
    },

    getVideo(id) {

    },

    addVideo(video) {
        
    },

    updateVideo(video) {

    },

    removeVideo(id) {

    },

    readFromStorage(callback) {
        fs.readFile('App_Data/videos.json', 'utf8', function (err, data) {
            callback(JSON.parse(data) || {});
         });
    },

    saveToStorage() {
        fs.writeFile('App_Data/videos.json', JSON.stringify(this.videos || {}), function (err) {
            if (err) {
                throw err;
            }
          });
    }
}